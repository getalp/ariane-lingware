NOMPLU:  SOUSN(C)-E-NCOM -ET- NUM(C)-E-PLU                                      
    ==   ECG(1) / ETAT(C):=ETAT(1) / / (S), (AN), FIN.                          
                                                                                
NEGADJ:  SOUSA(C)-E-ADNOM -ET- ASSERT(C)-E-NEG                                  
    ==   ECG(1) / ETAT(C):=ETAT(1) / / (NEGA), (AN), FIN.                       
                                                                                
COMPSUP: SOUSA(C)-E-ADNOM -ET- DEG(C)-NE-DEG0                                   
    ==   ECG(1) / ETAT(C):=ETAT(1) / / (WEREST), (EREST), (AN), FIN.            
                                                                                
PRES3S:  MODENG(C)-INC-IND -ET- TEMP(C)-E-PRES -ET- PERS(C)-E-3 -ET-NUM(C)-E-SIN
    ==   ECG(1) / ETAT(C):=ETAT(1) / / (S), FIN.                                
                                                                                
PRES:    MODENG(C)-INC-IND -ET- TEMP(C)-E-PRES                                  
    ==   ECG(1) / ETAT(C):=ETAT(1) / / FIN.                                     
                                                                                
PAST:    SOUSV(C)-E-PAPA -OU- MODENG(C)-INC-IND -ET- TEMP(C)-INC-PRTP           
    ==   ECG(1) / ETAT(C):=ETAT(1) / / (DOUBLE), (ED), (AN), FIN.               
                                                                                
GER:     MODENG(C)-E-GER -OU- SOUSV(C)-E-PAPR                                   
    ==   ECG(1) / ETAT(C):=ETAT(1) / /(DOUBLE),(ING), (EING), (AN), FIN.        
                                                                                
PONTU:   CAT(C)-E-PONCT == ECG(1) / T:=S;   S:=T   / / .                        
                                                                                
OTHERS:  NIL == ECD(1) / ETAT(C):=ETAT(1)  / / (AN), FIN.                       
                                                                                
                                                                                
                                                                                
S:       ETAT(C)-NE-F == ECD(2) / / TCHAINE(M,-1,"YE","IE") / .                 
                                                                                
NEGA:    NIL == ECG(2) / /                                                      
              TCHAINE(M,-1,"NL","NN");   TCHAINE(M,-1,"NM","MM");               
              TCHAINE(M,-1,"NR","RR");   TCHAINE(M,-1,"NP","MP") / .            
                                                                                
WEREST:  ETAT(C)-E-W  == ECD(2) / / TCHAINE(M,-1,"G","GG") / .                  
                                                                                
EREST:   ETAT(C)-NE-F -ET- ETAT(C)-NE-W == ECD(2) / / / .                       
                                                                                
DOUBLE:  ETAT(C)-E-W  == ECD(2) / /                                             
              TCHAINE(M,-1,"P","PP");   TCHAINE(M,-1,"L","LL");                 
              TCHAINE(M,-1,"T","TT") / .                                        
                                                                                
ED:      ETAT(C)-NE-F -ET- ETAT(C)-NE-W == ECD(2) / /                           
          TCHAINE(M,-1,"YED","IED"); TCHAINE(M,-1,"EED","ED") / .               
                                                                                
ING:     ETAT(C)-NE-F -ET- ETAT(C)-NE-E -ET- ETAT(C)-NE-W ==                    
         ECD(2) / / / .                                                         
                                                                                
EING:    ETAT(C)-E-E == ECD(2) / / TCHAINE(M,-1,"EI","I") / .                   
                                                                                
AN:      ETAT(P)-E-A                                                            
    ==   / T:=S / TCHAINE(G,0,"A A","AN A");   TCHAINE(G,0,"A E","AN E");       
                  TCHAINE(G,0,"A I","AN I");   TCHAINE(G,0,"A O","AN O");       
                  TCHAINE(G,0,"A U","AN U");   TCHAINE(G,0,"A Y","AN Y");       
                  TCHAINE(G,0,"A H","AN H") / .                                 
                                                                                
FIN:     NIL == / S:=T / / .                                                    
                                                                                
MOTINC:  NIL ==  / S:=T / / .                                                   
