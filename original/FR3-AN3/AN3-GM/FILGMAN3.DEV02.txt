BEAT                    ==VBISP   /        /S,                                  
                        ==VING    /        /ING,                                
                        ==        /        /.                                   
CARRY                   ==VBISP   /        /IES,                                
                        ==VING    /        /YING,                               
                        ==VEDR    /        /IED,                                
                        ==        /        /Y.                                  
CAST                    ==VBISP   /        /S,                                  
                        ==VING    /        /ING,                                
                        ==        /        /.                                   
COME                    ==VBISP   /        /ES,                                 
                        ==VING    /        /ING,                                
                        ==        /        /E.                                  
COMMIT                  ==VBISP   /        /S,                                  
                        ==VING    /DOUBLE  /ING,                                
                        ==VEDR    /DOUBLE  /ED,                                 
                        ==        /        /.                                   
CUT                     ==VBISP   /        /S,                                  
                        ==VING    /DOUBLE  /ING,                                
                        ==        /        /.                                   
FISH                    ==VBISP   /        /ES,                                 
                        ==VING    /        /ING,                                
                        ==VEDR    /        /ED,                                 
                        ==        /        /.                                   
NOTE                    ==VBISP   /        /ES,                                 
                        ==VING    /        /ING,                                
                        ==VEDR    /        /ED,                                 
                        ==        /        /E.                                  
RUN                     ==VBISP   /        /S,                                  
                        ==VING    /DOUBLE  /ING,                                
                        ==        /        /.                                   
WORK                    ==VBISP   /        /S,                                  
                        ==VING    /        /ING,                                
                        ==VEDR    /        /ED,                                 
                        ==        /        /.                                   

