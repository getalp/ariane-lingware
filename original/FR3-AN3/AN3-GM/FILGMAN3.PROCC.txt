A       01==CAT-E-A.                                                            
AB      01==CAT-E-A -ET- TYPOG-INC-ABBR.                                        
ABP     01==CAT-E-N -ET- NB-E-PLUR -ET- TYPOG-INC-ABBR.                         
ABS     01==CAT-E-N -ET- NB-E-SING -ET- TYPOG-INC-ABBR.                         
ACOUNT  01==CAT-E-A -ET- NCOUNT-E-COUNT.                                        
ADV     01==CAT-E-A -ET- SUBA-E-ADV.                                            
AMA     01==CAT-E-A-ET-AMLANG-E-US.                                             
AMAB    01==CAT-E-A -ET- TYPOG-INC-ABBR-ET-AMLANG-E-US.                         
AMABP   01==CAT-E-N -ET- NB-E-PLUR -ET- TYPOG-INC-ABBR-ET-AMLANG-E-US.          
AMABS   01==CAT-E-N -ET- NB-E-SING -ET- TYPOG-INC-ABBR-ET-AMLANG-E-US.          
AMACOUNT01==CAT-E-A -ET- NCOUNT-E-COUNT-ET-AMLANG-E-US.                         
AMADV   01==CAT-E-A -ET- SUBA-E-ADV-ET-AMLANG-E-US.                             
AMAMASS 01==CAT-E-A -ET- NCOUNT-E-MASS-ET-AMLANG-E-US.                          
AMASS   01==CAT-E-A -ET- NCOUNT-E-MASS-ET-AMLANG-E-US.                          
AMCARD  01==CAT-E-A -ET- SUBA-E-CARD -ET- TYPOG-INC-DIGIT-ET-AMLANG-E-US.       
AMCOMPAR01==CAT-E-A -ET- DEG-E-COMP-ET-AMLANG-E-US.                             
AMDAM   01==CAT-E-A -ET- POTDRV-E-AM-ET-AMLANG-E-US.                            
AMDAN   01==CAT-E-N -ET- POTDRV-E-AN-ET-AMLANG-E-US.                            
AMDCOUNT01==CAT-E-D -ET- NCOUNT-E-COUNT-ET-AMLANG-E-US.                         
AMDMASS 01==CAT-E-D -ET- NCOUNT-E-MASS-ET-AMLANG-E-US.                          
AMDNA   01==CAT-E-A -ET- POTDRV-E-NA-ET-AMLANG-E-US.                            
AMDNPA  01==CAT-E-A -ET- POTDRV-E-NPA-ET-AMLANG-E-US.                           
AMDNXA  01==CAT-E-A -ET- POTDRV-E-NXA-ET-AMLANG-E-US.                           
AMDVAA  01==CAT-E-A -ET- POTDRV-E-VAA-ET-AMLANG-E-US.                           
AMDVAAM 01==CAT-E-A -ET- POTDRV-E-VAAM-ET-AMLANG-E-US.                          
AMDVAAN 01==CAT-E-N -ET- POTDRV-E-VAAN-ET-AMLANG-E-US.                          
AMDVN   01==CAT-E-N -ET- POTDRV-E-VN-ET-AMLANG-E-US.                            
AMDVNG  01==CAT-E-V -ET- POTDRV-E-VNG-ET-AMLANG-E-US.                           
AMDVNP  01==CAT-E-N -ET- POTDRV-E-VN-ET-AMLANG-E-US-ET-NB-E-PLUR.               
AMDVPA  01==CAT-E-A -ET- POTDRV-E-VPA-ET-AMLANG-E-US.                           
AMDVPAM 01==CAT-E-A -ET- POTDRV-E-VPAM-ET-AMLANG-E-US.                          
AMDVPAN 01==CAT-E-N -ET- POTDRV-E-VPAN-ET-AMLANG-E-US.                          
AMGEN   01==ROLE-E-GEN-ET-AMLANG-E-US.                                          
AMLANG  01==AMLANG-E-US.                                                        
AMNAB   01==CAT-E-N -ET- TYPOG-INC-ABBR-ET-AMLANG-E-US.                         
AMNF    01==CAT-E-N -ET- GNR-E-FEM-ET-AMLANG-E-US.                              
AMNP    01==CAT-E-N-ET-NB-E-PLUR-ET-AMLANG-E-US.                                
AMOBJ   01==ROLE-E-OBJ-ET-AMLANG-E-US.                                          
AMORD   01==CAT-E-A   -ET- SUBA-E-ORD-ET-AMLANG-E-US.                           
AMORDD  01==CAT-E-A -ET- SUBA-E-ORD -ET- TYPOG-INC-DIGIT-ET-AMLANG-E-US.        
AMSUPER 01==CAT-E-A -ET- DEG-E-SUP-ET-AMLANG-E-US.                              
AMVBISP 01==CAT-E-V -ET- SUBVA-E-VB -ET- NB-E-SING -ET- MOOD-E-IND -ET-         
AMVBISP 02  TENSE-E-PRES -ET- PERS-E-3-ET-AMLANG-E-US.                          
AMVEDIR 01==CAT-E-V -ET- TENSE-E-PAST-ET-AMLANG-E-US.                           
AMVEDR  01==CAT-E-V -ET- SUBVA-E-PAP -ET- AMLANG -E- US -OU- CAT-E-V            
AMVEDR  02  -ET-TENSE -E- PAST -ET- AMLANG-E-US.                                
AMVEN   01==CAT-E-V -ET- SUBVA-E-PAP-ET-AMLANG-E-US.                            
AMVING  01==CAT-E-V -ET- SUBVA-E-PRP-ET-AMLANG-E-US.                            
CARD    01==TYPOG-INC-DIGIT.                                                    
COMPAR  01==CAT-E-A -ET- DEG-E-COMP.                                            
DAM     01==CAT-E-A -ET- POTDRV-E-AM.                                           
DAN     01==CAT-E-N -ET- POTDRV-E-AN.                                           
DCOUNT  01==CAT-E-D -ET- NCOUNT-E-COUNT.                                        
DMASS   01==CAT-E-D -ET- NCOUNT-E-MASS.                                         
DNA     01==CAT-E-A -ET- POTDRV-E-NA.                                           
DNPA    01==CAT-E-A -ET- POTDRV-E-NPA.                                          
DNXA    01==CAT-E-A -ET- POTDRV-E-NXA.                                          
DP      01==CAT-E-D -ET- NB-E-PLUR.                                             
DPOSI   01==CAT-E-D -ET- ROLE-E-GEN -ET- PERS -E- PERS0.                        
DPOS1P  01==CAT-E-D -ET- NB-E-PLUR -ET-PERS-E-1 -ET- ROLE-E-GEN.                
DPOS1S  01==CAT-E-D -ET- NB-E-SING -ET-PERS-E-1 -ET- ROLE-E-GEN.                
DPOS2   01==CAT-E-D -ET- PERS-E-2 -ET- ROLE-E-GEN.                              
DPOS3P  01==CAT-E-D -ET- NB-E-PLUR -ET-PERS-E-3 -ET- ROLE-E-GEN.                
DPOS3SF 01==CAT-E-D -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-GEN -ET-            
DPOS3SF 02  GNR-E-FEM.                                                          
DPOS3SM 01==CAT-E-D -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-GEN -ET-            
DPOS3SM 02  GNR-E-MAS.                                                          
DVAA    01==CAT-E-A -ET- POTDRV-E-VAA.                                          
DVAAM   01==CAT-E-A -ET- POTDRV-E-VAAM.                                         
DVAAN   01==CAT-E-N -ET- POTDRV-E-VAAN.                                         
DVN     01==CAT-E-N -ET- POTDRV-E-VN.                                           
DVNG    01==CAT-E-V -ET- POTDRV-E-VNG.                                          
DVNP    01==CAT-E-N -ET- POTDRV-E-VN -ET- NB-E-PLUR.                            
DVPA    01==CAT-E-A -ET- POTDRV-E-VPA.                                          
DVPAM   01==CAT-E-A -ET- POTDRV-E-VPAM.                                         
DVPAN   01==CAT-E-N -ET- POTDRV-E-VPAN.                                         
GEN     01==ROLE-E-GEN.                                                         
KI      01==CAT-E-S -ET- KGOV-E-INFCL.                                          
KINEG   01==CAT-E-S -ET- KGOV-E-INFCL -ET- NEG-E-NOT.                           
KN      01==CAT-E-S -ET- KGOV-E-NP.                                             
KP      01==CAT-E-S -ET- KGOV-E-PARTCL.                                         
KS      01==CAT-E-S -ET- KGOV-E-SCL.                                            
NAB     01==CAT-E-N -ET- TYPOG-INC-ABBR.                                        
NF      01==CAT-E-N -ET- GNR-E-FEM.                                             
NN      01==CAT-E-N.                                                            
NP      01==CAT-E-N-ET-NB-E-PLUR.                                               
NPS     01==CAT-E-N-ET-NB-E-PLUR-U-SING.                                        
OBJ     01==ROLE-E-OBJ.                                                         
ORD     01==CAT-E-A   -ET- SUBA-E-ORD.                                          
ORDD    01==CAT-E-A -ET- SUBA-E-ORD -ET- TYPOG-INC-DIGIT.                       
PERS1   01==PERS-E-1.                                                           
RPG1P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-1 -ET- ROLE-E-GEN.                
RPG1S   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-1 -ET- ROLE-E-GEN.                
RPG2    01==CAT-E-R -ET-PERS-E-2 -ET- ROLE-E-GEN.                               
RPG3FS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-GEN  -ET-           
RPG3FS  02  GNR-E-FEM.                                                          
RPG3MS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-GEN  -ET-           
RPG3MS  02  GNR-E-MAS.                                                          
RPG3P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-3 -ET- ROLE-E-GEN.                
RPG3S   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-GEN  -ET-           
RPG3S   02  GNR-NE-MAS -ET- GNR-NE-FEM.                                         
RPO1P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-1 -ET- ROLE-E-OBJ.                
RPO1S   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-1 -ET- ROLE-E-OBJ.                
RPO2    01==CAT-E-R -ET-PERS-E-2 -ET- ROLE-E-OBJ.                               
RPO3FS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-OBJ  -ET-           
RPO3FS  02  GNR-E-FEM.                                                          
RPO3MS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-OBJ  -ET-           
RPO3MS  02  GNR-E-MAS.                                                          
RPO3P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-3 -ET- ROLE-E-OBJ.                
RPS1P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-1 -ET- ROLE-E-SUBJ.               
RPS1S   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-1 -ET- ROLE-E-SUBJ.               
RPS2    01==CAT-E-R -ET-PERS-E-2 -ET- ROLE-E-SUBJ.                              
RPS3FS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-SUBJ -ET-           
RPS3FS  02  GNR-E-FEM.                                                          
RPS3MS  01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- ROLE-E-SUBJ -ET-           
RPS3MS  02  GNR-E-MAS.                                                          
RPS3P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-3 -ET- ROLE-E-SUBJ.               
RR1P    01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-1.                                
RR1S    01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-1.                                
RR2P    01==CAT-E-R -ET- PERS-E-2  -ET- NB-E-PLUR .                             
RR2S    01==CAT-E-R -ET- PERS-E-2  -ET- NB-E-SING .                             
RR3FS   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET-  GNR-E-FEM.                
RR3MS   01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET- GNR-E-MAS .                
RR3P    01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-3.                                
RR3S    01==CAT-E-R -ET- NB-E-SING -ET-PERS-E-3 -ET-                            
RR3S    02  GNR-NE-MAS -ET- GNR-NE-FEM .                                        
SUPER   01==CAT-E-A -ET- DEG-E-SUP.                                             
TYPAC   01==TYPOG-INC-TCAP.                                                     
TYPDIG  01==TYPOG-INC-DIGIT.                                                    
V       01==CAT-E-V.                                                            
VAM     01==CAT-E-V -ET- SUBVA-E-VB -ET- NB -E-SING -ET- MOOD-E-IND             
VAM     02  -ET- PERS-E-1 -ET- TENSE-E-PRES.                                    
VBIPP   01==CAT-E-V -ET- SUBVA-E-VB -ET- MOOD-E-IND -ET- NB-E-PLUR -ET-         
VBIPP   02  TENSE-E-PRES.                                                       
VBIPPT  01==CAT-E-V -ET- SUBVA-E-VB -ET- NB -E-PLUR -ET- MOOD-E-IND -ET-        
VBIPPT  02  TENSE-E-PAST -ET- PERS-E-3.                                         
VBISP   01==CAT-E-V -ET- SUBVA-E-VB -ET- NB-E-SING -ET- MOOD-NE-SUB -ET-        
VBISP   02  TENSE-E-PRES -ET- PERS-E-3.                                         
VBISPT  01==CAT-E-V -ET- SUBVA-E-VB -ET- NB -E-SING -ET- MOOD-E-IND -ET-        
VBISPT  02  TENSE-E-PAST -ET- PERS -E- 3.                                       
VBSSPT  01==CAT-E-V -ET- SUBVA-E-VB -ET- NB -E-SING -ET- MOOD-E-SUB -ET-        
VBSSPT  02  TENSE-E-PAST -ET- PERS-E-3.                                         
VEDIR   01==CAT-E-V -ET- TENSE-E-PAST.                                          
VEDR    01==CAT-E-V -ET- SUBVA-E-PAP -OU- CAT-E-V -ET-TENSE -E-PAST.            
VEN     01==CAT-E-V -ET- SUBVA-E-PAP.                                           
VING    01==CAT-E-V -ET- SUBVA-E-PRP.                                           
XAB     01==CAT-E-A -ET- TYPOG-INC-ABBR-U-TCAP.                                 
XABP    01==CAT-E-N -ET- NB-E-PLUR -ET- TYPOG-INC-ABBR-U-TCAP.                  
XABS    01==CAT-E-N -ET- NB-E-SING -ET- TYPOG-INC-ABBR-U-TCAP.                  
XAMAB   01==CAT-E-A -ET- TYPOG-INC-ABBR-U-TCAP-ET-AMLANG-E-US.                  
XAMABP  01==CAT-E-N -ET- NB-E-PLUR -ET- TYPOG-INC-ABBR-U- TCAP                  
XAMABP  02  -ET-AMLANG-E-US.                                                    
XAMABS  01==CAT-E-N -ET- NB-E-SING -ET- TYPOG-INC-ABBR-U-TCAP                   
XAMABS  02  -ET-AMLANG-E-US.                                                    
XAMLANG 01==AMLANG-E-US -ET- TYPOG -INC- TCAP.                                  
XAMNAB  01==CAT-E-N -ET- TYPOG-INC-ABBR-U-TCAP-ET-AMLANG-E-US.                  
XAMNF   01==CAT-E-N -ET- GNR-E-FEM-ET-AMLANG-E-US-ET-TYPOG-INC-TCAP.            
XAMNP   01==CAT-E-N-ET-NB-E-PLUR-ET-AMLANG-E-US-ET-TYPOG-INC-TCAP.              
XDCOUNT 01==CAT-E-D -ET- NCOUNT-E-COUNT-ET- TYPOG-INC-TCAP.                     
XDMASS  01==CAT-E-D -ET- NCOUNT-E-MASS -ET- TYPOG-INC-TCAP.                     
XDNPA   01==CAT-E-A -ET- POTDRV-E-NPA -ET- TYPOG-INC-TCAP.                      
XDP     01==CAT-E-D -ET- NB-E-PLUR -ET- TYPOG -INC-TCAP.                        
XKI     01==CAT-E-S -ET- KGOV-E-INFCL -ET-TYPOG-INC-TCAP.                       
XKINEG  01==CAT-E-S -ET- KGOV-E-INFCL -ET- NEG-E-NOT-ET-TYPOG-INC-TCAP.         
XKN     01==CAT-E-S -ET- KGOV-E-NP-ET- TYPOG-INC-TCAP.                          
XKP     01==CAT-E-S -ET- KGOV-E-PARTCL -ET- TYPOG-INC-TCAP.                     
XKS     01==CAT-E-S -ET- KGOV-E-SCL-ET- TYPOG-INC-TCAP.                         
XNAB    01==CAT-E-N -ET- TYPOG-INC-ABBR-U-TCAP.                                 
XNF     01==CAT-E-N -ET- GNR-E-FEM-ET-TYPOG-INC-TCAP.                           
XNP     01==CAT-E-N-ET-NB-E-PLUR-ET-TYPOG-INC-TCAP.                             
XRR3P   01==CAT-E-R -ET- NB-E-PLUR -ET-PERS-E-3 -ET- TYPOG-INC-TCAP.            

