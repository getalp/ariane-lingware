                                                                                
                                  -EXC-                                         
                                                                                
$NEG     ==(1).                                                                 
$TYPE    ==(COP,MODAL).                                                         
$INTR    ==(1,2).                                                               
$MCUL    ==(ELID,SUBS).                                                         
$RL      ==(         **LOGICAL RELATIONS.                                       
            ARG0,    **FIRST PLACE OF ARGUMENT.                                 
            ARG1,    **SECOND PLACE OF ARGUMENT.                                
            ARG2,    **THIRD PLACE OF ARGUMENT.                                 
            ARG01,   **EITHER FIRST OR SECOND PLACE.                            
            ARG12,   **EITHER SECOND OR THIRD PLACE.                            
            TRL0,    **TRANSFER TO ARGUMENT 0.                                  
            TRL1,    **TRANSFER TO ARGUMENT 1.                                  
            ID,      **THE SAME RELATION AS THE FATHER NODE(IDENTICAL).         
            SMARG,   **SEMI-ARGUMENT OR LEXEME FOR PARTICULARISATION.           
            GRA0,    ** THE GOVERNOR IS ARG0, ARG1, ARG2 RESPECTIVELY.          
            GRA1,    **     OF THE GOVERNED NODE;   NEW 4/84.                   
            GRA2 ).                                                             
                                                                                
$RS      ==(         **SEMANTIC RELATIONS.                                      
            CAUSE,   **CAUSE OF SOME EVENT OR PROCESS.                          
            SOURCE,  **MADE OUT OF.                                             
            EMIT,    **AGENT OF A PROCESS OR EXPERIENCER.                       
            COND,    **CONDITION.                                               
            AIM,     **AIM IN LOCATION,TIME.                                    
            FINAL,   **AIM IN ACTION.                                           
            RECEPT,  **RECEIVER OR BENEFICIARY.                                 
            RESULT,  **RESULT OF A PROCESS.                                     
            CONSEQ,  **CONSEQUENCE.                                             
            TOPIC,   **TOPIC.                                                   
            TOY,     **OBJECT OR ANIMATE SUBMITTED TO SOME ACTION.              
            INST,    **CONCRETE INSTRUMENT.                                     
            ACCOMP,  **IN COMPANY OF.                                           
            ANALOG,  **REFERENCE FOR COMPARISON.                                
            CONCES,  **CONCESSIVE.                                              
            QFIER,   **QUANTIFIER.                                              
            QFOBJ,   **QUANTIFIED OBJECT.                                       
            PROX,    **DEGREE OF APPROXIMATION.                                 
            LOCAL,   **LOCAL TIME OR SPACE.                                     
            QUAL,    **QUALIFICATION.                                           
            ID).     **THE SAME RELATION AS THE FATHER NODE(IDENTICAL).         
$VOICE   ==(PAS).                                                               
$SLOCK   ==(1,2,3).                                                             
$LOCK1   ==(1).                                                                 
$LOCK2   ==(1).                                                                 
$BECL    ==(         **CLAUSE GOVERNED BY A COPULA.                             
            EQ,      **EQUALITY:OXYGEN IS A GAS.                                
            PROP).   **PROPERTY:THIS EXPERIMENT IS DANGEROUS.                   
                                                                                
$K       ==(         **MORPHO-SYNTACTIC CLASSES.                                
            NP,      **NOUN PHRASE.                                             
            AP,      **ADJECTIVE PHRASE.                                        
            ADVP,    **ADVERBIAL PHRASE.                                        
            CARDP,   **CARDINAL PHRASE.                                         
            VCL,     **VERBAL CLAUSE.                                           
            RELCL,   **RELATIVE CLAUSE.                                         
            SCL,     **SUBORDINATE CLAUSE.                                      
            PARTCL,  **GERUND OR PARTICIPIAL OR INFINITIVE CLAUSE.              
            VK).     **VERBAL KERNEL.                                           
                                                                                
$SF      ==(         **SYNTACTIC FUNCTIONS.                                     
            SUBJ,                                                               
            OBJ1,                                                               
            OBJ2,                                                               
            FSUBJ,                                                              
            ATSUBJ,                                                             
            ATOBJ,                                                              
            ATG,                                                                
            DES,                                                                
            CIRC,                                                               
            CPAG,                                                               
            COMP,                                                               
            COORD,                                                              
            REG,                                                                
            GOV,                                                                
            AUX,                                                                
            JUXT,                                                               
            APP,                                                                
            INC,                                                                
            LXAX,                                                               
            REFDG).                                                             
                                                                                
$TDRV    ==(         **THAI DERIVATIONS.                                        
            AN,                                                                 
            AM,                                                                 
            VA1,                                                                
            VA2,                                                                
            VA3,                                                                
            VAM1,                                                               
            VAM2,                                                               
            VAM3,                                                               
            VN1,                                                                
            VN2,                                                                
            VNAG,                                                               
            VNI).                                                               
$THUK    ==(Y).       **CAN BE PRECEDED BY 'THUK' FOR PASSIVE.                  
$POSIT   ==(B4,AFTER).                                                          
$TCAUSE  ==(1).                                                                 
$TSUBN   ==(CN,PN).                                                             
$TSUBA   ==(ADJ,ADV,INT,ORD,CARD,ADCL).                                         
$TSUBD   ==(DEM,INT,QTF).                                                       
$TSUBR   ==(PERS,DEM,REL,INT,QTF,INDEF).                                        
$TSUBS   ==(PREP,CONJ).                                                         
$TDEG    ==(REL,COMP1,COMP2,SUP,CONS).                                          
$TASP    ==(PROG,ACH,GOING).                                                    
$CDEL    ==(1).                                                                 
KLOCK    ==(1).                                                                 
SFLOCK   ==(1).                                                                 
CATLOCK  ==(1).                                                                 
CREATE   ==(SCL,PARTCL,NP,RELCL).                                               
                                                                                
                                                                                
                        -NEX-                                                 
                                                                                
                                                                                
                                                                                
$NUM     ==(SIN,PLU).                                                           
$DEG     ==(REL,COMP,SUP,CONS).                                                 
$TENSE   ==(PRES,PAST,PERF,PPERF,FUT,COND).                                     
$SEM     ==(       ** SEMANTIC CLASSES.                                         
           TIME,   ** TIME.                                                     
           ANIM,   ** ANIMATED.                                                 
           LOC,    ** LOCATION.                                                 
           MEAS,   ** MEASUREMENT.                                              
           PHY,    ** MEASURABLE QUANTITY OR PHENOMENA.                         
           PROC,   ** PROCESS.                                                  
           CONC,   ** CONCRETE PHYSICAL OBJECT.                                 
           ABST).  ** ABSTRACT NOUN OR PROCESS.                                 
$SEMV    ==(STATE,PROC,APROC).                                                  
$SEMTI   ==(DATE,DURAT).                                                        
$SEMAN   ==(HUMAN,PERS,PLAN).                                                   
$SEMCO   ==(SUBST,OBJ).                                                         
                                                                                
$SEMZ    ==(       ** SEMANTIC OF ARG0.                                         
           TIME, ANIM, LOC, MEAS, PHY, PROC, CONC, ABST ).                      
                                                                                
$SEM1    ==(       ** SEMANTIC OF ARG1.                                         
           TIME, ANIM, LOC, MEAS, PHY, PROC, CONC, ABST ).                      
$SEM2    ==(TIME,ANIM,LOC,MEAS,PHY,PROC,CONC,ABST).                             
$SMARG   ==(JUXT,OF).                                                           
                                                                                
** THAI DV.                                                                     
                                                                                
$TCAT    ==(V,N,A,D,R,S,C,P,ASP,NMOD,NEG,CLASS).                                
                                                                                
$TVL1    ==(        ** THAI SYNTACTIC VALENCIES.                                
           N,       ** NOUN.                                                    
           KIEOKAP, ** ABOUT.                                                   
           THI,     ** AT, TO.                                                  
           DOI,     ** BY.                                                      
           PHUA,    ** FOR.                                                     
           CHAK,    ** FROM.                                                    
           NAI,     ** IN.                                                      
           SU,      ** INTO, ONTO, TO.                                          
           KHONG,   ** OF.                                                      
           BON,     ** ON.                                                      
           NUA,     ** OVER.                                                    
           KAP,     ** WITH.                                                    
           TO,      ** TO.                                                      
           V,       ** VERB.                                                    
           SUB).    ** SUBORDINATE CLAUSE.                                      
$TVL2    ==(N,KIEOKAP,THI,DOI,PHUA,CHAK,NAI,SU,KHONG,BON,NUA,KAP,TO,            
            V,SUB).                                                             
$FDRV    ==(AN,AM,VA1,VA2,VA3,VAM1,VAM2,VAM3,VN1,VN2,VNAG,VNI).                 



-FIN-



